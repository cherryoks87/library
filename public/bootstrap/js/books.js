var category = null;
var byTitle = null;
var byAuthor = null;
var books = null;

function sendResponse(data) {
    data.category = category;
    data.by_author = byAuthor;
    data.by_title = byTitle;

    $.ajax({
        url:'/books/ajax',
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        success: function (data)
        {
            $( "#dialog-message" ).dialog( "close" );
            $('div#listOfBooks').html(data.output);
            $('#mainTitle').html(data.title);
            $('ul.page-header-breadcrumb').html(data.category);
            books = data.books;
        }
    });
}

$(document).ready(function () {
    sendResponse({});
    $(document).on('click', '.pagination a', function () {
        sendResponse({"page": $(this).text()});
        return false;
    });

    $(document).on('click', 'a.buttonCategory', function () {
        category = $(this).data('id');
        byTitle = null; byAuthor = null; //reset search filters
        sendResponse({});
        return false;
    });

    $(document).on('click', '#searchForm .primary-button', function(){
        byTitle = $('#searchByTitle').val();
        byAuthor = $('#searchByAuthor').val();
        sendResponse({ 'search': true });
        $('button.search-close').click();
        return false;
    });

    $(document).on('click', "a.openBookInfo", function() {
        $('#dialog-message').html(books[$(this).data('id')]);
        $('#dialog-message .moreInfo').removeAttr('hidden');
        $('#dialog-message').dialog( "open" );
        return false;
    });
});

$( function() {
    $( "#dialog-message" ).dialog({
        autoOpen: false,
        height: 500,
        width: 900,
        modal: true,
        buttons: [
            {
                text: "Ok",
                click: function() {
                    $( this ).dialog( "close" );
                }
            },
        ]
    });
} );