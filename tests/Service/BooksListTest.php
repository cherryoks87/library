<?php

use PHPUnit\Framework\TestCase;
use App\Service\BooksList;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\Connection;

class BooksListTest extends TestCase
{
	private $jsonObject;

	protected function setUp()
	{
		$this->jsonObject = json_encode((object) [
			'items' => [
				(object) [
					'id' => '1',
					'volumeInfo' => (object) [
						'title' => 'test title',
						'description' => 'description',
						'imageLinks' => (object) [
							'thumbnail' => 'image.jpg'
						],
						'authors' => (object) [
							'author 1',
							'author 2'
						]
					]

				]
			],
			'totalItems' => 1
		]);
		parent::setUp(); // TODO: Change the autogenerated stub
	}

	public function testCreateBook()
	{
		$connectionMock = $this
			->getMockBuilder(Connection::class)
			->disableOriginalConstructor()
			->getMock();
		$connectionMock->expects($this->any())
			->method('beginTransaction')
			->willReturn(null);
		$connectionMock->expects($this->any())
			->method('commit')
			->willReturn(null);
		$connectionMock->expects($this->any())
			->method('rollBack')
			->willReturn(null);

		$repositoryMock = $this
			->getMockBuilder(EntityRepository::class)
			->setMethods(['getListOfIds', 'findAll'])
			->disableOriginalConstructor()
			->getMock();

		$repositoryMock->expects($this->any())
			->method('findAll')
			->willReturn((object)[]);
		$repositoryMock->expects($this->any())
			->method('getListOfIds')
			->willReturn([]);

		$entityManagerMock = $this->createMock(EntityManager::class);
		$entityManagerMock->expects($this->any())
			->method('getRepository')
			->willReturn($repositoryMock);
		$entityManagerMock->expects($this->any())
			->method('getConnection')
			->willReturn($connectionMock);

		$booksList = (new BooksList($entityManagerMock))->init($this->jsonObject);
		$books = $booksList->getList();
		$this->assertTrue(count($books) == 1);
	}
}