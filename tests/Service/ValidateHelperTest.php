<?php
// tests/Service/ValidateHelperTest.php
namespace App\Tests\Service;

use App\Service\ValidateHelper;
use PHPUnit\Framework\TestCase;

class ValidateHelperTest extends TestCase
{
	public function testFilterSearchString()
	{
		$helper = new ValidateHelper();
		$helper->addCheck('param1', 'test% str&ing1', ValidateHelper::TYPE_VALID_SEARCH_STRING, []);
		$helper->addCheck('param2', 'Edward Waymire', ValidateHelper::TYPE_VALID_SEARCH_STRING, []);
		$helper->addCheck('param3', '    	edward waymire   ', ValidateHelper::TYPE_VALID_SEARCH_STRING, []);
		$data = $helper->run()->getValidData();

		//check that all parameters existed
		$this->assertArrayHasKey('param1', $data);
		$this->assertArrayHasKey('param2', $data);
		$this->assertArrayHasKey('param3', $data);

		//check that array doesn't have other parameters
		$this->assertCount(3, $data);

		//check filtered string
		$this->assertEquals('test string1', $data['param1']); //special symbols was deleted
		$this->assertEquals('edward waymire', $data['param2']); //uppercase letters was converted to lowercase
		$this->assertEquals('edward waymire', $data['param3']); //extra spaces are removed
	}

	public function testValidateMaxLength()
	{
		$helper1 = new ValidateHelper();
		$helper1->addCheck('param1', 'test', ValidateHelper::TYPE_VALID_SEARCH_STRING, [
			ValidateHelper::TYPE_MAX_VALUE => 10,
		])->addCheck('param2', 'teststring', ValidateHelper::TYPE_VALID_SEARCH_STRING, [
			ValidateHelper::TYPE_MAX_VALUE => 10,
		]);
		//check that string corresponds to the condition
		$this->assertFalse($helper1->run()->hasErrors());

		$helper2 = new ValidateHelper();
		$helper2->addCheck('param3', 'teststring1', ValidateHelper::TYPE_VALID_SEARCH_STRING, [
			ValidateHelper::TYPE_MAX_VALUE => 10,
		]);
		//check that string doesn't correspond to the condition
		$this->assertTrue($helper2->run()->hasErrors());
	}

	public function testValidateNotEmptyWith()
	{
		$helper1 = new ValidateHelper();
		$helper1->addCheck('param1', 'test', ValidateHelper::TYPE_VALID_SEARCH_STRING, [
			ValidateHelper::TYPE_NOT_EMPTY_WITH => 'param2',
		])->addCheck('param2', 'teststring', ValidateHelper::TYPE_VALID_SEARCH_STRING, []);

		//check that strings correspond to the condition (both strings are not empty)
		$this->assertFalse($helper1->run()->hasErrors());

		$helper2 = new ValidateHelper();
		$helper2->addCheck('param1', 'test', ValidateHelper::TYPE_VALID_SEARCH_STRING, [
			ValidateHelper::TYPE_NOT_EMPTY_WITH => 'param2',
		])->addCheck('param2', '', ValidateHelper::TYPE_VALID_SEARCH_STRING, []);

		//check that strings correspond to the condition (first string are not empty)
		$this->assertFalse($helper2->run()->hasErrors());

		$helper3 = new ValidateHelper();
		$helper3->addCheck('param1', '', ValidateHelper::TYPE_VALID_SEARCH_STRING, [
			ValidateHelper::TYPE_NOT_EMPTY_WITH => 'param2',
		])->addCheck('param2', 'teststring', ValidateHelper::TYPE_VALID_SEARCH_STRING, []);

		//check that strings correspond to the condition (second string are not empty)
		$this->assertFalse($helper3->run()->hasErrors());

		$helper4 = new ValidateHelper();
		$helper4->addCheck('param1', '', ValidateHelper::TYPE_VALID_SEARCH_STRING, [
			ValidateHelper::TYPE_NOT_EMPTY_WITH => 'param2',
		])->addCheck('param2', '', ValidateHelper::TYPE_VALID_SEARCH_STRING, []);

		//check that strings don't corresponds to the condition (both strings are empty)
		$this->assertTrue($helper4->run()->hasErrors());
	}
}