<?php
use PHPUnit\Framework\TestCase;
use App\Service\Request;

class RequestTest extends TestCase
{
	public function testRun()
	{
		$request1 = new Request('https://www.googleapis.com/books/v1/volumes', 'get', [
			'q' => 'a'
		]);
		//check correct response
		$this->assertTrue($request1->getStatus() == '200');

		$response1 = $request1->getResponse();
		$this->assertFalse(empty($response1));

		$object1 = json_decode($response1);
		$this->assertTrue(isset($object1->items));

		$request2 = new Request('https://www.googleapis.com/books/v1/volumes', 'get', []);
		//check incorrect response (wrong params)
		$this->assertFalse($request2->getStatus() == '200');

		$request3 = new Request('https://www.googleapis.com', 'get', []);
		//check incorrect response (wrong url)
		$this->assertFalse($request3->getStatus() == '200');
	}
}