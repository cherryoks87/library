<?php
if (isset($_ENV['BOOTSTRAP_RESET_DATABASE']) && $_ENV['BOOTSTRAP_RESET_DATABASE'] == true) {
	echo "Resetting test database...";
	passthru(sprintf('php "%s/../bin/console" cache:clear -e "test" --no-debug --no-interaction',__DIR__));
	passthru(sprintf('php "%s/../bin/console" doctrine:database:drop -e "test" --force --no-debug --no-interaction',__DIR__));
	passthru(sprintf('php "%s/../bin/console" doctrine:database:create -e "test" --no-debug --no-interaction',__DIR__));
	passthru(sprintf('php "%s/../bin/console" doctrine:migration:migrate -e "test" -n --no-debug --no-interaction',__DIR__));
	passthru(sprintf('php "%s/../bin/console" doctrine:fixtures:load -e "test" -n --no-debug --no-interaction',__DIR__));
	echo " Done" . PHP_EOL . PHP_EOL;
}
require __DIR__.'/../vendor/autoload.php';
