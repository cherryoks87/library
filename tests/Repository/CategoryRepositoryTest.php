<?php

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CategoryRepositoryTest extends KernelTestCase
{
	/**
	 * @var \Doctrine\ORM\EntityManager
	 */
	private $entityManager;

	protected function setUp()
	{
		$kernel = self::bootKernel();

		$this->entityManager = $kernel->getContainer()
			->get('doctrine')
			->getManager();
	}

	public function testGetBiggestCategories()
	{
		//check count of items
		$categories = $this->entityManager
			->getRepository(\App\Entity\Category::class)
			->getBiggestCategories();
		$this->assertCount(2, $categories);

		//check order by max value
		$arrayOfCounts = [];
		foreach ($categories as $category) {
			$arrayOfCounts[] = $category['cnt'];
		}
		$firstCategoryCnt = $categories[0]['cnt'];
		$this->assertTrue($firstCategoryCnt == max($arrayOfCounts));
	}

	protected function tearDown()
	{
		parent::tearDown();

		$this->entityManager->close();
		$this->entityManager = null; // avoid memory leaks
	}
}