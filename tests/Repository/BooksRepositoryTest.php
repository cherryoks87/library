<?php

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Entity\Book;

class BooksRepositoryTest extends KernelTestCase
{
	/**
	 * @var \Doctrine\ORM\EntityManager
	 */
	private $entityManager;

	protected function setUp()
	{
		$kernel = self::bootKernel();

		$this->entityManager = $kernel->getContainer()
			->get('doctrine')
			->getManager();
	}

	public function testCreateBook()
	{
		$filter = [];

		//check search without any filters
		$books = $this->entityManager
			->getRepository(Book::class)
			->makeQueryForSearchBooks($filter)->getResult();
		;
		$this->assertCount(26, $books);

		//check by category
		$category = $this->entityManager
			->getRepository(\App\Entity\Category::class)
			->findOneBy(['name' => 'category1']);
		$filter = [
			'category' => $category->getId()
		];
		$books = $this->entityManager
			->getRepository(Book::class)
			->makeQueryForSearchBooks($filter)->getResult();
		;
		$this->assertCount(20, $books);

		//check by author
		$filter = [
			'by_author' => 'author2'
		];
		$books = $this->entityManager
			->getRepository(Book::class)
			->makeQueryForSearchBooks($filter)->getResult();
		;
		$this->assertCount(5, $books);

		//check by title
		$filter = [
			'by_title' => 'magazine'
		];
		$books = $this->entityManager
			->getRepository(Book::class)
			->makeQueryForSearchBooks($filter)->getResult();
		;
		$this->assertCount(5, $books);

		//check by author and title
		$filter = [
			'by_title' => 'special',
			'by_author' => 'author1'
		];
		$books = $this->entityManager
			->getRepository(Book::class)
			->makeQueryForSearchBooks($filter)->getResult();
		;
		$this->assertCount(1, $books);

		//check by author, title and category (true)
		$category2 = $this->entityManager
			->getRepository(\App\Entity\Category::class)
			->findOneBy(['name' => 'category2']);
		$filter = [
			'by_title' => 'special',
			'by_author' => 'author1',
			'category' => $category2->getId()
		];
		$books = $this->entityManager
			->getRepository(Book::class)
			->makeQueryForSearchBooks($filter)->getResult();
		;
		$this->assertCount(1, $books);

		//check by author, title and category (false category - 0 items)
		$filter = [
			'by_title' => 'special',
			'by_author' => 'author1',
			'category' => $category->getId()
		];
		$books = $this->entityManager
			->getRepository(Book::class)
			->makeQueryForSearchBooks($filter)->getResult();
		;
		$this->assertCount(0, $books);
	}

	public function testGetListOfIds()
	{
		$ids = $this->entityManager
			->getRepository(Book::class)
			->getListOfIds();
		$this->assertCount(5, $ids);
	}

	public function testGetBooksByAuthors()
	{
		$author = $this->entityManager
			->getRepository(\App\Entity\Author::class)
			->findOneBy(['name' => 'Author2']);
		$books = $this->entityManager
			->getRepository(Book::class)
			->getBooksByAuthors([$author->getId()], 10);
		$this->assertCount(5, $books);

		//check count
		$count = 10;
		$author2 = $this->entityManager
			->getRepository(\App\Entity\Author::class)
			->findOneBy(['name' => 'Author1']);
		$books = $this->entityManager
			->getRepository(Book::class)
			->getBooksByAuthors([$author2->getId()], $count);
		$this->assertCount($count, $books);
	}

	protected function tearDown()
	{
		parent::tearDown();

		$this->entityManager->close();
		$this->entityManager = null; // avoid memory leaks
	}
}