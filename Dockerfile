FROM registry.gitlab.com/cherryoks87/library/builder

ADD . /var/www/library
WORKDIR /var/www/library
RUN composer install
