<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class for work with table 'category'
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }

	/**
	 * Get categories and order them by count of books inside them.
	 * You can point count of getting values.
	 * @param null|integer $count Count of getting values. If you don't set this param you get all array with category name and count of books
	 * @return mixed
	 */
    public function getBiggestCategories($count = null)
	{
		//select name, count(bc.book_id) as cnt from category c join book_category bc on bc.category_id = c.id group by c.name order by cnt desc;
		$query = $this->createQueryBuilder('c');
		if(empty($count)) {
			$query->select('c.id, c.name, count(b.id) as cnt');
		} else {
			$query->setMaxResults($count);
		}
		return $query->leftJoin('c.books', 'b')
			->groupBy('c.id')
			->orderBy('count(b.id)', 'DESC')
		 	->getQuery()
			->getResult()
		;

	}
}
