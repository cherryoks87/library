<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class for work with table 'books'
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BooksRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Book::class);
    }

	/**
	 * Make query for search books using filters
	 * @param array $filter
	 * @return \Doctrine\ORM\Query
	 */
    public function makeQueryForSearchBooks(array $filter)
	{
		$query = $this->createQueryBuilder('b');

		$andWhere = false; //Point if we need function andWhere instead of where
		if(!empty($filter)) {
			//filter by category
			if(!empty($filter['category'])) {
				$query->leftJoin('b.categories', 'c')
					->where('c.id = :category')
					->setParameter('category', $filter['category']);
				$andWhere = true;
			}
			//filter by author
			if(!empty($filter['by_author'])) {
				$query->leftJoin('b.authors', 'a');
				$where = 'lower(a.name) LIKE :by_author';
				if($andWhere) {
					$query->andWhere($where);
				} else {
					$query->where($where);
				}
				$query->setParameter('by_author', '%' . strtolower($filter['by_author']) . '%');
				$query->groupBy('b.id');
				$andWhere = true;
			}
			//filter by title
			if(!empty($filter['by_title'])) {
				$where = 'lower(b.title) LIKE :by_title';
				if($andWhere) {
					$query->andWhere($where);
				} else {
					$query->where($where);
				}
				$query->setParameter('by_title', '%' . strtolower($filter['by_title']) . '%');
			}
		}
		return $query->orderBy('b.title', 'ASC')->getQuery();
	}

	/**
	 * Get List of loading ids
	 * @return array
	 */
	public function getListOfIds()
	{
		$result = $this->createQueryBuilder('b')->select('b.id_number')->getQuery()->getResult();
		if(empty($result)) return [];
		$ids = [];
		foreach ($result as $value) {
			if(!empty($value['id_number'])) $ids[] = $value['id_number'];
		}
		return $ids;
	}

	/**
	 * Prepare query for get books by this authors
	 * @param $ids Author's ids
	 * @param $count int Count of items
	 * @return mixed
	 */
	public function getBooksByAuthors($ids, $count)
	{
		return $this->createQueryBuilder('b')
			->innerJoin('b.authors', 'a')
			->where('a.id IN (' . implode(', ', $ids) . ')')
			->setMaxResults($count)
			->getQuery()
			->getResult();
	}
}
