<?php
// src/Command/LoadBooks.php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\Request;
use App\Service\BooksList;

/**
 * This command allows you to load test list of books from GoogleApi. You need to clear your database before use it.
 * Class LoadBooksCommand
 * @package App\Command
 */
class LoadBooksCommand extends Command
{
	/**
	 * @var string URL for Google API (is taken from config file - config/services.yaml)
	 */
    private $url;

	/**
	 * @var \Doctrine\ORM\EntityManagerInterface Manager for work with Database
	 */
    private $entityManager;

	/**
	 * Count items in one loading request
	 */
    const COUNT_ITEMS_FOR_LOAD = 40; //Can't be more 40!

	/**
	 * LoadBooksCommand constructor.
	 * @param \Doctrine\ORM\EntityManagerInterface $entityManager
	 * @param $urlLoadBooks
	 */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $entityManager, $urlLoadBooks) {
        $this->entityManager = $entityManager;
    	$this->url = $urlLoadBooks;

        parent::__construct();
    }

	/**
	 * Command configuration
	 */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:load-books')

            // the short description shown while running "php bin/console list"
            ->setDescription('Load list of books from GoogleApi.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to load list of books from GoogleApi. You need to clear your database before use it.')

			->addArgument('q', InputArgument::OPTIONAL, 'String of search.');
    }

	/**
	 * Command execution
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int|null|void
	 * @throws \Exception
	 */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
		$startIndex = 0;
		$count = 0;
		while($startIndex <= $count) {
			//Prepare request
    		$params = [
				'q' => $input->getArgument('q') ?: 'a', //By default load books which contain 'a' letter
				'printType' => 'books', //Load only books witout magazines and etc.
				'maxResults' => self::COUNT_ITEMS_FOR_LOAD, //Count items in one loading request
				'startIndex' => $startIndex //Start index in this loading request
			];
			$request = new Request($this->url, 'GET', $params);

			if ($request->getStatus() != "200") {
				throw new \Exception($request->getError());
			}
			//Use service BooksList for parsing response and save data
			$init = (new BooksList($this->entityManager))->init($request->getResponse());
			//Prepare next request
			$count = $init->getCount();
			$startIndex = $startIndex + self::COUNT_ITEMS_FOR_LOAD;

		} ;

		$output->writeln('Loading finished');
    }
}