<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Book;
use App\Entity\Category;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Service\ValidateHelper;

/**
 * Show list of books and searches by name and authors.
 * Class BooksController
 * @package App\Controller
 */
class BooksController extends AbstractController
{

	/**
	 * This pre-action do checks and prepare data fo view
	 * @param Request $request
	 * @param PaginatorInterface $paginator
	 * @return array
	 * @throws \Exception
	 */
	private function init(Request $request, PaginatorInterface $paginator) {
		$session = new Session();
		$validateHelper = new ValidateHelper();
		$params = [];
		$viewData = [];
		$viewData['search'] = $request->request->getBoolean('search');
		$page = $request->query->getInt('page') ?: $request->request->getInt('page');
		//Get data for search from session when we use pager after search (and search post data already empty)
		if ($request->isMethod('get') && $page > 0) {
			$params = [
				'by_author' => $session->get('by_author'),
				'by_title' => $session->get('by_title')
			];
		} else {
			//remove filter when we on main page
			$session->remove('by_author');
			$session->remove('by_title');
		}

		//get, filter and validate post data from search
		if ($request->isMethod('post')) {
			$byAuthor = $request->request->get('by_author');
			$byTitle = $request->request->get('by_title');
			//set requirements for getting values
			$validationListTitle = [ValidateHelper::TYPE_MAX_VALUE => 1000];
			$validationListAuthor = $validationListTitle;
			if($viewData['search']) {
				$validationListAuthor[ValidateHelper::TYPE_NOT_EMPTY_WITH] = 'by_title';
			}
			$validateHelper
				->addCheck('by_author', $byAuthor,ValidateHelper::TYPE_VALID_SEARCH_STRING,
					$validationListAuthor)
				->addCheck('by_title', $byTitle, ValidateHelper::TYPE_VALID_SEARCH_STRING,
					$validationListTitle)->run(); //do chech and filter
			//if everything is ok save params into session and get filter data
			if(!$validateHelper->hasErrors()) {
				$session->set('by_author', $byAuthor);
				$session->set('by_title', $byTitle);
				$params = $validateHelper->getValidData();
			}
		}

		//if we come here from /category page we need to get category for search
		$categoryId = $request->attributes->getInt('id') ?: $request->request->getInt('category');
		if(!empty($categoryId)) {
			$params['category'] = $categoryId;
			$viewData['category'] = $this->getDoctrine()->getRepository(Category::class)->find($categoryId);
		}

		//prepare data for view
		$repository = $this->getDoctrine()->getRepository(Book::class);
		$query = $repository->makeQueryForSearchBooks($params);
		$pagination = $paginator->paginate($query, $request->query->getInt('page', $page ?: 1),15);

		$viewData['pagination'] = $pagination; //all finding books
		$viewData['errors'] = $validateHelper->getErrors(); //if we have validation errors
		$viewData['topCategories'] = $this->getDoctrine()->getRepository(Category::class)
			->getBiggestCategories(5); //list of biggest categories for top menu

		return $viewData;
	}

	/**
	 * Full list of books and search for them.
	 * @Route("/books", name="books_list")
	 * @param PaginatorInterface $paginator
	 * @param int $page
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function index(Request $request, PaginatorInterface $paginator)
	{
		return $this->render('books/index.html.twig', $this->init($request, $paginator));
	}

	/**
	 * List of books by selected category and search for them.
	 * @Route("/books/category/{id}", name="category_show")
	 * @param Request $request
	 * @param PaginatorInterface $paginator
	 * @param $id
	 * @param int $page
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function category(Request $request, PaginatorInterface $paginator, $id)
	{
		return $this->render('books/category.html.twig', $this->init($request, $paginator));
	}

	/**
	 * @Route("/books/ajax", name="ajax")
	 * @param Request $request
	 * @param PaginatorInterface $paginator
	 * @return JsonResponse
	 */
	public function ajax(Request $request, PaginatorInterface $paginator)
	{
		$data = $this->init($request, $paginator);

		$booksViews = [];
		//render all books pages
		foreach($data['pagination'] as $item) {
			$booksViews[$item->getId()] = $this->renderView('default/row.html.twig', [
				'book' => $item
			]);
		}

		$output = [
			'output' => $this->renderView('books/rows.html.twig', $data),
			'title' => $this->renderView('books/title.html.twig', $data),
			'category' => $this->renderView('books/breadcrambs_category.html.twig', $data),
			'books' => $booksViews
		];
		return new JsonResponse($output);
	}
}
