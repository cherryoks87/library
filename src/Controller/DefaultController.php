<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Book;
use App\Entity\Category;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;

class DefaultController extends AbstractController
{
	const COUNT_OF_CATEGORIES_ON_MAIN_MENU = 5;
	const COUNT_OF_OTHER_BOOKS_BY_THIS_AUTHOR = 10;

	/**
	 * Pre-action function for prepere data for view.
	 * @return array
	 */
	private function init()
	{
		return [
			'topCategories' => $this->getDoctrine()->getRepository(Category::class)
				->getBiggestCategories(self::COUNT_OF_CATEGORIES_ON_MAIN_MENU) //list of biggest categories for top menu
		];
	}

	public function index()
	{

	}

	/**
	 * Show full information about book.
	 * @Route("/book/{id}", name="book_show")
	 * @param Request $request
	 * @param $id
	 * @param PaginatorInterface $paginator
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function book(Request $request, $id, PaginatorInterface $paginator)
	{
		$params = $this->init();
		//prepare information about selected book
		$params['book'] = $this->getDoctrine()->getRepository(Book::class)->find($id);
		$authorIds = $params['book']->getAuthorIds();
		//prepare list of categories and count of books in them
		$params['allCategories'] = $this->getDoctrine()->getRepository(Category::class)
			->getBiggestCategories();
		//prepare list of other books this authors
		$params['otherBooks'] = $this->getDoctrine()->getRepository(Book::class)
			->getBooksByAuthors($authorIds, self::COUNT_OF_OTHER_BOOKS_BY_THIS_AUTHOR);

		return $this->render('default/book.html.twig', $params);
	}

	/**
	 * Page about site author
	 * @Route("/about", name="about")
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function about()
	{
		return $this->render('default/about.html.twig', $this->init());
	}
}