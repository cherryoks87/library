<?php
namespace App\DataFixtures;

use App\Entity\Book;
use App\Entity\Author;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
	public function load(ObjectManager $manager)
	{
		$author = new Author();
		$author->setName('Author1');
		$manager->persist($author);

		$author2 = new Author();
		$author2->setName('Author2');
		$manager->persist($author2);

		$category = new Category();
		$category->setName('category1');
		$manager->persist($category);

		$category2 = new Category();
		$category2->setName('category2');
		$manager->persist($category2);

		for ($i = 0; $i < 20; $i++) {
			$book = new Book();
			$book->setTitle('book '.$i);
			$book->addAuthor($author);
			$book->addCategory($category);
			$manager->persist($book);
		}

		for ($i = 0; $i < 5; $i++) {
			$book = new Book();
			$book->setIdNumber('num'.($i+1));
			$book->setTitle('magazine '.$i);
			$book->addAuthor($author2);
			$book->addCategory($category2);
			$manager->persist($book);
		}

		$book = new Book();
		$book->setTitle('special');
		$book->addAuthor($author);
		$book->addCategory($category2);
		$manager->persist($book);

		$manager->flush();
	}
}