<?php

namespace App\Exception;

/**
 * Exceptions used in services
 * Class ServiceException
 * @package App\Exception
 */
class ServiceException extends \Exception
{
	//ValidateHelper exception codes
	const WRONG_NAME_IN_CONDITION = 101;
	const NOT_SET_FIELDS_FOR_CHECK = 102;
	const WRONG_TYPE_OF_FILTRATION = 103;
	const WRONG_TYPE_OF_VALIDATION = 104;

	//Request exception codes
	const INVALID_METHOD = 201;
	const URL_NOT_PROVIDED = 202;

	//BookList exception codes
	const EMPTY_RESPONSE = 301;
	const WRONG_RESPONSE_STRUCTURE = 302;

	private static $errorMessages = [
		//ValidateHelper exception messages
		self::WRONG_NAME_IN_CONDITION => 'Wrong name of field in condition',
		self::NOT_SET_FIELDS_FOR_CHECK => 'You must add fields for check and filter',
		self::WRONG_TYPE_OF_FILTRATION => 'Wrong type of filtration',
		self::WRONG_TYPE_OF_VALIDATION => 'Wrong type of validation',
		//Request exception messages
		self::INVALID_METHOD => 'Invalid method %s',
		self::URL_NOT_PROVIDED => 'URL not provided',

		//BookList exception messages
		self::EMPTY_RESPONSE => 'Empty response',
		self::WRONG_RESPONSE_STRUCTURE => 'Wrong response structure'
	];

	public function __construct($code, $param = null)
	{
		parent::__construct(sprintf(self::$errorMessages[$code], [$param]), $code, null);
	}

}