<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category entity
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

	/**
	 * @ORM\ManyToMany(targetEntity="Book", mappedBy="categories")
	 */
	private $books;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

	/**
	 * Get list of books
	 * @return mixed
	 */
	public function getBooks()
	{
		return $this->books;
	}

	/**
	 * Add list of books
	 * @param $books
	 */
	public function setBooks($books)
	{
		$this->books = $books;
	}
}
