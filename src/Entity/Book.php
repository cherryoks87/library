<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Book entity
 * @ORM\Entity(repositoryClass="App\Repository\BooksRepository")
 */
class Book
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=12, nullable=true)
     */
    private $id_number;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $publisher;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $publisher_date;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $language;

	/**
	 * @ORM\ManyToMany(targetEntity="Author", inversedBy="books")
	 * @ORM\JoinTable(name="books_authors")
	 */
	private $authors;

	/**
	 * @ORM\ManyToMany(targetEntity="Category", inversedBy="books")
	 * @ORM\JoinTable(name="books_categories")
	 */
	private $categories;

	public function __construct()
	{
		$this->authors = new \Doctrine\Common\Collections\ArrayCollection();
		$this->categories = new \Doctrine\Common\Collections\ArrayCollection();
		return $this;
	}

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdNumber(): ?string
    {
        return $this->id_number;
    }

    public function setIdNumber(?string $id_number): self
    {
        $this->id_number = $id_number;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getShortDescription()
	{
		return \mb_strimwidth($this->getDescription(), 0, 200, '...');
	}

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPublisher(): ?string
    {
        return $this->publisher;
    }

    public function setPublisher(?string $publisher): self
    {
        $this->publisher = $publisher;

        return $this;
    }

    public function getPublisherDate(): ?string
    {
        return $this->publisher_date;
    }

    public function setPublisherDate(?string $publisher_date): self
    {
        $this->publisher_date = $publisher_date;

        return $this;
    }

    public function getImage(): ?string
    {
        return str_replace('http://', 'https://', $this->image);
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): self
    {
        $this->language = $language;

        return $this;
    }

	/**
	 * Get information about authors
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getAuthors()
	{
		return $this->authors;
	}

	/**
	 * Prepare information about authors as a string
	 * @return null|string
	 */
    public function getAuthorString(): ?string
	{
		$authors = [];
		foreach ($this->getAuthors() as $author) {
			$authors[] = $author->getName();
		}

		return implode(', ', $authors);
	}

	/**
	 * Prepare ids of authors
	 * @return array
	 */
	public function getAuthorIds()
	{
		$ids = [];
		foreach ($this->getAuthors() as $author) {
			$ids[] = $author->getId();
		}
		return $ids;
	}

	/**
	 * Add new author for saving
	 * @param Author|null $author
	 */
	public function addAuthor(Author $author = null)
	{
		$this->authors->add($author);
	}

	/**
	 * Find author for deleting
	 * @param Author $author
	 */
	public function removeAuthor(Author $author)
	{
		$this->authors->removeElement($author) ;
	}

	/**
	 * Add list of authors
	 * @param $authors
	 */
	public function setAuthors($authors)
	{
		$this->authors = $authors;
	}

	/**
	 * Get information about categories
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getCategories()
	{
		return $this->categories;
	}

	/**
	 * Prepare information about categories as a string
	 * @return null|string
	 */
	public function getCategoryString(): ?string
	{
		$categories = [];
		foreach ($this->getCategories() as $category) {
			$categories[] = $category->getName();
		}

		return implode(', ', $category);
	}

	/**
	 * Add new category for saving
	 * @param Category|null $category
	 */
	public function addCategory(Category $category = null)
	{
		$this->categories->add($category);
	}

	/**
	 * Find category for deleting
	 * @param Category $category
	 */
	public function removeCategory(Category $category)
	{
		$this->categories->removeElement($category) ;
	}

	/**
	 * Add list of categories
	 * @param $categories
	 */
	public function setCategories($categories)
	{
		$this->categories = $categories;
	}
}
