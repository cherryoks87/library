<?php
namespace App\Service;
use App\Exception\ServiceException;

/**
 * Service for send request to API
 * Class Request
 * @package App\Service
 */
class Request
{
	/**
	 * @var string URL to API
	 */
    private $url;
	/**
	 * @var string Method (POST or GET)
	 */
    private $method;
	/**
	 * @var array Query
	 */
    private $data;

    private $response;
    private $status;
    private $error;

    function __construct($url, $method, $data = []) {
        $method = strtolower($method);
        if (in_array($method, ['post', 'get'])) {
            $this->method = $method;
        } else {
            throw new ServiceException(ServiceException::INVALID_METHOD, $method);
        }
        if (empty($url)) {
            throw new ServiceException(ServiceException::URL_NOT_PROVIDED);
        }
        $this->url  = $url;
        $this->data = $data;

        $this->run();
    }

	/**
	 * Do request
	 */
    private function run()
    {
        if ($this->method == 'get' && !empty($this->data)) {
            $this->url .= "?" . http_build_query($this->data);
        }

        $c = curl_init();

        curl_setopt($c, CURLOPT_URL, $this->url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER ,true);
        curl_setopt($c, CURLOPT_HTTPHEADER, array('Expect:'));

        //preparing post data
        if($this->method == 'post') {
            curl_setopt($c, CURLOPT_POST, true);
            curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($this->data));
        }

        $this->response = curl_exec($c);
        $this->status = curl_getinfo($c, CURLINFO_HTTP_CODE);
        $this->error = curl_errno($c) . ": ". curl_error($c);
        curl_close($c);
    }

    function getStatus() {
        return $this->status;
    }

    function getResponse() {
        return $this->response;
    }

    function getError() {
        return $this->error;
    }
}