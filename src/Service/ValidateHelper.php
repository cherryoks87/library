<?php
namespace App\Service;
use App\Exception\ServiceException;

/**
 * Service for filter and validate input data
 * Class ValidateHelper
 * @package App\Service
 */
class ValidateHelper
{
	/**
	 * @var array Prepared filtered data
	 */
	private $validParams = [];
	/**
	 * @var array Input Data
	 */
	private $inputData = [];
	/**
	 * @var array Error messages
	 */
	private $errors = [];

	//Types of filters
	const TYPE_VALID_SEARCH_STRING = 1; //Admits only letters, numbers and space

	//Types of validation
	const TYPE_MAX_VALUE = 2; //Checks data on max length string
	const TYPE_NOT_EMPTY_WITH = 3; //Checks on non-empty - at least on: current field or other setting field must be not empty

	//Names of functions
	private $filterFunctions = [
		self::TYPE_VALID_SEARCH_STRING => 'filterSearchString'
	];
	private $validateFunctions = [
		self::TYPE_MAX_VALUE => 'validateMaxLength',
		self::TYPE_NOT_EMPTY_WITH => 'validateNotEmptyWith'
	];

	//Error messages
	const MESSAGE_FOR_MAX_LENGTH = 'Search string is too long';
	const MESSAGE_FOR_NOT_EMPTY_WITH = 'Please enter a search string';

	/**
	 * Add error in list
	 * @param $message
	 */
	private function setError($message) {
		$this->errors[] = $message;
	}

	/**
	 * Get all errors
	 * @return array
	 */
	public function getErrors() {
		return $this->errors;
	}

	/**
	 * Check errors
	 * @return bool
	 */
	public function hasErrors() {
		return !empty($this->errors);
	}

	/**
	 * Add fields in validation list
	 * @param $name 		string 	Name of field
	 * @param $value 		mixed 	Value of field
	 * @param $type 		int 	Type field for filter
	 * @param $requirements array 	List of requirements
	 * $requirements = [
	 * 		{type of validation} => {parameter for validation}
	 * ]
	 * @return $this
	 */
    public function addCheck($name, $value, $type, $requirements) {
    	$this->inputData[$name] = [
    		'value' => $value,
			'type' => $type,
			'requirements' => $requirements
		];
    	return $this;
	}

	//----------Functions for filter---------------
	/**
	 * Admits only letters, numbers and space
	 * @param $value
	 * @return string
	 */
	private function filterSearchString($value) {
		$value = trim(strtolower(preg_replace('/[^a-zA-Z0-9 \t]/', '', $value)));
		return $value;
	}

	//----------Functions for validation------------
	/**
	 * Checks data on max length string
	 * @param $value
	 * @param $condition int Count of possible letters
	 */
	private function validateMaxLength($value, $condition) {
		if(strlen($value) > $condition) {
			$this->setError(self::MESSAGE_FOR_MAX_LENGTH);
		}
	}

	/**
	 * Checks on non-empty two fields - one of the fields (current field or other setting field) must be not empty
	 * @param $value
	 * @param $condition string Name of second field for checking
	 * @throws ServiceException
	 */
	private function validateNotEmptyWith($value, $condition) {
    	if(!isset($this->inputData[$condition])) throw new ServiceException(ServiceException::WRONG_NAME_IN_CONDITION);
    	if(empty($value) && empty($this->inputData[$condition]['value'])) {
    		$this->setError(self::MESSAGE_FOR_NOT_EMPTY_WITH);
		}
	}

	/**
	 * Do filtration and validation
	 * @return $this
	 * @throws ServiceException
	 */
    public function run()
    {
    	if(empty($this->inputData)) throw new ServiceException(ServiceException::NOT_SET_FIELDS_FOR_CHECK);
    	foreach ($this->inputData as $name => $config) {
    		if(!array_key_exists($config['type'], $this->filterFunctions)) throw new ServiceException(ServiceException::WRONG_TYPE_OF_FILTRATION);

    		//Make filtration of value
			$this->validParams[$name] = $this->{$this->filterFunctions[$config['type']]}($config['value']);

			//Make validation of value
    		if(!empty($config['requirements'])) {
    			foreach ($config['requirements'] as $type => $condition) {
					if(!array_key_exists($type, $this->validateFunctions)) throw new ServiceException(ServiceException::WRONG_TYPE_OF_VALIDATION);
					$this->{$this->validateFunctions[$type]}($this->validParams[$name], $condition);
				}
			}
		}
		return $this;
    }

	/**
	 * Get valid data after filtration
	 * @return array
	 */
    public function getValidData() {
    	return $this->validParams;
	}


}