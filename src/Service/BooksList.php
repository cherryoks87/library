<?php
namespace App\Service;
use App\Entity\Book;
use App\Entity\Author;
use App\Entity\Category;
use App\Exception\ServiceException;

/**
 * Service for parse response from API and save information about books in database.
 * Class BooksList
 * @package App\Service
 */
class BooksList
{
	/**
	 * @var array List of getting books
	 */
    private $list = [];

	public function getList(){
		return $this->list;
	}

	private $entityManager;

    function __construct(\Doctrine\ORM\EntityManagerInterface $entityManager){
		$this->entityManager = $entityManager;
		return $this;
    }

	/**
	 * @var string Response json string
	 */
    private $response;

	/**
	 * @var int Count of getting elements
	 */
    private $listCount;

	public function getCount(){
		return $this->listCount;
	}

	/**
	 * Get response and try to parse it and save data
	 * @param $response
	 * @return $this
	 * @throws ServiceException
	 */
    public function init($response) {
		if(empty($response)) {
			throw new ServiceException(ServiceException::EMPTY_RESPONSE);
		}

		$this->response = json_decode($response);
		if(!isset($this->response->items)) {
			throw new ServiceException(ServiceException::WRONG_RESPONSE_STRUCTURE);
		}

		$this->listCount = $this->response->totalItems;

		//get existing categories from database
		$this->setCategoryList();
		//get existing authors from database
		$this->setAuthorList();
		//get existing books from database
		$this->setBookIds();

		foreach ($this->response->items as $item) {
			if(isset($item->volumeInfo)
				&& isset($item->volumeInfo->authors)
				&& isset($item->volumeInfo->title)
				&& isset($item->volumeInfo->description)
				&& isset($item->volumeInfo->imageLinks)
				&& strlen($item->volumeInfo->title) <= 255
				&& !in_array($item->id, $this->bookIds)
			) {

				//save info
				$book = $this->createNewBook($item);
				$this->list[] = $book;
			}

		}
		return $this;
	}

	/**
	 * @var array List of existing categories
	 */
	private $categoryList = [];
    private function setCategoryList() {
		$categories = $this->entityManager->getRepository(Category::class)->findAll();
		foreach ($categories as $category) {
			$this->categoryList[$category->getName()] = $category->getId();
		}

	}

	/**
	 * @var array List of existing authors
	 */
	private $authorList = [];
	private function setAuthorList() {
		$authors = $this->entityManager->getRepository(Author::class)->findAll();
		foreach ($authors as $author) {
			$this->authorList[$author->getName()] = $author->getId();
		}

	}

	/**
	 * @var array list of ids existing books
	 */
	private $bookIds = [];
	private function setBookIds() {
		$this->bookIds = $this->entityManager->getRepository(Book::class)->getListOfIds();
	}

	/**
	 * Create Book entity and save information to database
	 * @param $item
	 * @return Book
	 * @throws \Doctrine\DBAL\ConnectionException
	 */
    private function createNewBook($item) {
		$this->entityManager->getConnection()->beginTransaction();
		try {
			$book = new Book();
			$book->setIdNumber($item->id);
			$info = $item->volumeInfo;
			$book->setTitle($info->title);
			$book->setDescription($info->description ?? '');
			$book->setImage($info->imageLinks->thumbnail);
			$book->setPublisher($info->publisher ?? '');
			$book->setPublisherDate($info->publishedDate ?? '');

			foreach ($info->authors as $authorName) {
				$author = $this->addAuthor($authorName);
				$book->addAuthor($author);
			}

			if(isset($info->categories)) {
				foreach ($info->categories as $categoryName) {
					$category = $this->addCategory($categoryName);
					$book->addCategory($category);
				}
			}

			$this->entityManager->persist($book);
			$this->entityManager->flush();

			$this->entityManager->getConnection()->commit();
		} catch (\Exception $e) {
			$this->entityManager->getConnection()->rollBack();
			throw $e;
		}
		return $book;
	}

	/**
	 * Create Author entity and save information to database
	 * @param $authorName
	 * @return Author|null|object
	 */
	private function addAuthor($authorName) {
		if(!array_key_exists($authorName, $this->authorList)) {
			$author = new Author();
			$author->setName($authorName);
			$this->entityManager->persist($author);
			$this->entityManager->flush();
			$authorId = $author->getId();
			$this->authorList[$authorName] = $authorId;
		} else {
			$authorId = $this->authorList[$authorName];
			$author = $this->entityManager
				->getRepository(Author::class)->find($authorId);
		}
		return $author;
	}

	/**
	 * Create Category entity and save information to database
	 * @param $categoryName
	 * @return Category|null|object
	 */
	private function addCategory($categoryName) {
		if(!array_key_exists($categoryName, $this->categoryList)) {
			$category = new Category();
			$category->setName($categoryName);
			$this->entityManager->persist($category);
			$this->entityManager->flush();
			$categoryId = $category->getId();
			$this->categoryList[$categoryName] = $categoryId;
		} else {
			$categoryId = $this->categoryList[$categoryName];
			$category = $this->entityManager
				->getRepository(Category::class)->find($categoryId);
		}
		return $category;
	}
}