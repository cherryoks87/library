# Library

This site has ability to show list of books, find books by author or/and title, get books by category.
Besides it consider command for load some data about books from API

## Getting Started

These instructions will get you a copy of the project up and running on your local machine. 

### Prerequisites

For working this project you need to have PHP7 and PostgreSQL last version.

### Installing with Docker

DATABASE_URL consist address to postgres like this: pgsql://user:pass@127.0.0.1:5432/library

```
docker build -f Dockerfile.builder -t registry.gitlab.com/cherryoks87/library/builder .
docker build .
docker run -e DATABASE_URL $IMAGE_TAG php bin/console doctrine:migration:migrate --no-interaction
docker run -e DATABASE_URL --workdir /var/www/library/public $IMAGE_TAG
```

Webserver ROOT must point on ./public/

### Installing without Docker

DATABASE_URL consist address to postgres like this: pgsql://user:pass@127.0.0.1:5432/library
1. Install php:7.2 
2. Install PostgreSQL last version
3. Install php extensions: pdo pdo_pgsql zip.
4. Install composer.
5. Run composer install.

For create database and schema run:
```
php bin/console doctrine:database:create
php bin/console doctrine:migration:migrate 
```

## Running the tests
Install phpunit.
Before starting the tests, the database is cleared. If you don't want it, you can change parameter BOOTSTRAP_RESET_DATABASE in file phpunit.xml.dist

Tests run by command:

```
./bin/phpunit
```

## How it works
For loading data from Google API you need to go to project folder and run:
```
php bin/console app:load-books
```
By default this command load books with letter 'a' in title or author, if you want to load by your own query set parameter q:

```
php bin/console app:load-books q=Pratchet
```

Also you can add records directly to the database.

When you have data you can go to browser and load main page.
You will see list of books (15 for page), you can use the search. You can find them in the upper right corner. 
If you want to show books by category you can сlick on one of them.
On category page you also can use search and pagination.
If you click on book title or image you will get to book show page. You can see all information about it.
Besides that site have one more page - about me. You can find link in the lower right corner.


## Authors

* **Oksana Kukina** - *developer* - [Link](https://gitlab.com/cherryoks87/library)


## Acknowledgments

* Templates made with using Colorlib's examples. 
* For testing I used data from Google Api. 

