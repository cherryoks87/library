FROM php:7.2-fpm

RUN apt-get update && apt-get install -y libpq-dev zlib1g-dev git subversion bash patch && apt-get clean && rm -rf /var/lib/apt/lists/* && \
    docker-php-ext-install pdo pdo_pgsql zip && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    ln -snf /usr/share/zoneinfo/UTC /etc/localtime && echo UTC > /etc/timezone && printf '[PHP]\ndate.timezone = "UTC"\n' > /usr/local/etc/php/conf.d/tzone.ini
